% Vectorize using matrix and element-wise operators

%% Example: arbitrary matrix math

A = rand(200, 200);

% non-vectorized version
tic
B1 = zeros(size(A));
for i=1:size(A,1)
    for j=1:size(A,2)
       T = 0;
       for k=1:size(A,1)
           T = T + A(i,k)*A(j,k);
       end
       B1(i,j) = T * (A(i,j)/2) + 1;
    end
end
nonvec = toc

% vectorized version
tic
B2 = ((A*A') .* (A/2)) + 1;
vec = toc

% compare results
equal = approxeq(B1, B2)
ratio = nonvec / vec


