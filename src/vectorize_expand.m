% Vectorize using repmat and bsxfun to match matrix dimensions


%% Example: compute distance from one point to a vector of points

N = 1000;
x = rand(N,2);
y = rand(1,2);

% non-vectorized version
tic
d1 = zeros(N,1);
for ii = 1:N
	d(ii) = sqrt( (x(ii,:)-y).^2 );
end
nonvec = toc

% repmat version
tic
d2 = sqrt( (x-repmat(y, [N,1])).^2 );
rep = toc


% bsxfun version
tic 
d3 = sqrt( bsxfun(@minus, x, y).^2 );
bsx = toc;

% compare results
equal  = isequal(z1, z2, z3)
ratio_rep = nonvec/rep
ratio_bsx = nonvec/bsx


%% Example: evaluate the equation for a plane (z = ax + by + c)

a = 10;
b = 5; 
c = 1;
x = linspace(-3, 3, 5000);
y = linspace(-3, 3, 2500);

% non-vectorized version
tic
z1 = zeros(numel(y), numel(x));
for ii = 1:numel(y)
    for jj = 1:numel(x)
        z1(ii,jj) = a*x(jj)+b*y(ii)+c;
    end
end
nonvec = toc

% repmat version
tic
z2 = a*repmat(x, [numel(y), 1])+b*repmat(y', [1, numel(x)])+c;
rep = toc

% bsxfun version
tic
z3 = bsxfun(@plus, a*x, b*y')+c;
bsx = toc

% compare results
equal  = isequal(z1, z2, z3)
ratio_rep = nonvec/rep
ratio_bsx = nonvec/bsx

%% Example: "center" a 3D dataset by subtracting out the mean of the third dimension

A3d = rand(200,300,400);
A1 = A3d; 
A2 = A3d; 
A3 = A3d;

% non-vectorized version*
tic
m = mean(A1,3);
for i=1:size(A1,3)
   A1(:,:,i) = A1(:,:,i) - m;
end
nonvec = toc

% vectorized version using repmat
tic
A2 = A2 - repmat(mean(A2,3),[1,1,size(A2,3)]);
rep = toc

% vectorized version using bsxfun
tic
A3 = bsxfun(@minus,A3,mean(A3,3));
bsx = toc

equal = isequal(A1,A2,A3)
ratio_rep = nonvec/rep
ratio_bsx = nonvec/bsx

