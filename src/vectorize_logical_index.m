% Vectorize using logical indexing

%% Example: extract a subset of a variable 

A = rand(500, 500);

% non-vectorized version
tic
B1 = []; % note, it is difficult to preallocate here
counter = 1;
 for j=1:size(A,2)
     for i=1:size(A,1)
        if(A(i,j) < 0.2)
            B1(counter,1) = A(i,j);
            counter = counter + 1;
        end
    end
end
nonvec = toc

 % vectorized version
tic
B2 = A(A < 0.2);
vec = toc

% compare results

equal = isequal(B1,B2)
ratio = nonvec / vec


%% Example: extract and use a subset of several variables

d = rand(1e4, 1);
h = rand(1e4, 1);

% non-vectorized version
tic;
v1 = [ ];
for n = 1:numel(d)
	if h(n) > 0.5
		v1(end+1) = 1/12*pi*d(n)^2*h(n);
	end
end 
nonvec = toc

% vectorize version
tic;
mask = h>0.5;
v2 = 1/12*pi* d(mask).^2 .* h(mask);
vec = toc

% compare
equal = isequal(v1',v2)
ratio = nonvec / vec


%% Vectorize a loop including an if-else statement

A = rand(1e5,1);
B = rand(1e5,1);

% non-vectorized
tic
for i = 1:numel(A)
   if B(i)>0.5
      C1(i) = A(i)^2;
   else
      C1(i) = exp(B(i));
   end
end
nonvec = toc

% vectorized
tic
C2 = zeros(size(A));
gt = B>0.5;
C2(gt) = A(gt).^2;
C2(~gt) = exp(B(~gt));
vec = toc

% compare:
equal = isequal(C1,C2)
ratio = nonvec / vec
