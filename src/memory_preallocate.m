%% Preallocation for a matrix of known size

% A is not preallocated 
tic
for i = 1:30000
    A(i) = i;
end
without = toc

%  B is preallocated with the zeros command. 
tic
B = zeros(30000,1);     
for i = 1:30000
    B(i) = i;
end
with = toc
ratio = without / with

