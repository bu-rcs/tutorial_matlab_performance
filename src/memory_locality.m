% Making use of hardware optimizations for data locality 

% Example: populate an array element-by-element

n = 5e3; 

% bad version
tic
x1 = zeros(n);
for i = 1:n % rows
   for j = 1:n % columns
     x1(i,j) = i+(j-1)*n;
   end
end
bad = toc

% good version
tic
x2 = zeros(n);
for j = 1:n % columns
   for i = 1:n % rows
     x2(i,j) = i+(j-1)*n;
   end
end
good = toc

% compare results
equal = isequal(x1, x2);
ratio = bad/good