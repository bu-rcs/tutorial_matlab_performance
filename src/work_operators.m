% Reorganize equations to use fewer or more efficient operators
% 
% Basic operators have different speeds:
% 	
%   Add        3- 6 cycles
%   Multiply   4- 8 cycles
%   Divide     32-45 cycles
%   Power, etc	(worse)

% Example: two equivalent code snippetsusing inefficient and efficient operators

N = 1e4;
y = rand(N,1);

x1 = zeros(N,1);
v1 = zeros(N,1);
z1 = zeros(N,1);

x2 = zeros(N,1);
v2 = zeros(N,1);
z2 = zeros(N,1);

% bad
tic
c = 4;
for i=1:N
    x1(i) = y(i)/c;
    v1(i) = x1(i) + x1(i)^2 + x1(i)^3;
    z1(i) = log(x1(i)) + log(y(i));
end
bad = toc

% good
tic
s = 1/4;
for i=1:N
    x2(i) = y(i)*s;
    v2(i) = x2(i)*(1+x2(i)*(1+x2(i)));
    z2(i) = log(x2(i) * y(i));
end
good = toc

% compare results
equal_x = approxeq(x1, x2)
equal_v = approxeq(v1, v2)
equal_z = approxeq(z1, z2)
ratio = bad/good