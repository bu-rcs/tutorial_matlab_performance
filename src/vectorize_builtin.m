% Making use of built-on vectorized functions 

%% Example: compute the natural log of a data matrix

A = rand(500, 500);

% non-vectorized version
Bnv = zeros(size(A)); % preallocate output
tic                    
for i=1:size(A,1)
    for j=1:size(A,2);
        Bnv(i,j) = log(A(i,j));
    end
end
nonvec = toc

% vectorized version
tic
Bv = log(A);
vec = toc

% compare results 
equal = isequal(Bnv,Bv)
ratio = nonvec / vec


%% Distance between two points in N-dimensions

N = 300;
pt1 = rand(N,1);
pt2 = rand(N,2);

% non-vectorized version
tic
sq_dist = 0;
for ii = 1:N
	sq_dist = sq_dist+(pt1(ii)-pt2(ii))^2;
end
dist1 = sq_dist^0.5;
nonvec = toc

% vectorized version
tic
dist2 = sqrt(sum( (pt1-pt2).^2 ) );
vec = toc

% compare results
equal = isequal(dist1, dist2);
ratio = nonvec/vec;OA
