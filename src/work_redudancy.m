% Eliminate redundant work by moving constant expressions outside of loops

%% Example 1: fairly obvious redundant operations

n = 1e5;

% bad version
tic
d1 = zeros(n,1);
for i = 1:n
    a = 10;
    b = 20;
    d1(i) = sqrt(a*b)*rand(1)*c;
end
bad = toc

% good version
tic
d2 = zeros(n,1);
a = 10;
b = 20;
c = sqrt(a*b);
for i = 1:n   
    d2(i) = rand(1)*c;
end
good = toc

% compare results
equal = isequal(d1, d2);
ratio = bad/good
